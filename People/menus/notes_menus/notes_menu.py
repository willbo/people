from People.menus.main_menus.main_menu import soft_exit, MainMenu
from People.menus.menu_selection_dict import menu_builder


class NotesMenu:
	def __init__(self):
		self.loop = True

	def menu_prompt(self):
		while self.loop:
			for items in range(len(menu_builder('NOTES'))):
				print(menu_builder('NOTES')['select_' + str(items)]['text'])
			select = int(input("Selection: "))
			if select == 1:
				print('To be added: Classes')
				self.loop = False
			elif select == 2:
				print('To be added: Semester')
				self.loop = False
			elif select == 3:
				print('Back')
				MainMenu()
			elif select == 4:
				print('END PROGRAM')
				soft_exit()
			else:
				print('Enter Proper Selection')
				self.loop = True
