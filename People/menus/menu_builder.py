from . import menu_dict

def menu_text_builder(menu_key: str):
	"""
	To call dict:
	dict_class = MenuDict()
	dict_class.(MENU_NAME)['sub_name']['sub_sub_name']
	Ex:
	IN: dict_class = MenuDict()
	IN: dict_class.MainMenu['select_1']['title']
	OUT: math_selection_string
	"""
	for items in range(len(compiled_dict_builder(menu_key))):  # iterates through length of select numbers of built dict
		print(compiled_dict_builder(menu_key)['select_' + str(items)]['text'])
		# prints what: (NAME)_MENU_DICT['select_(num)'][(dict text)] calls

def menu_title_builder(menu_key: str, dict_return: bool):
	if dict_return:
		menu_title_dict = {}
		for items in range(len(compiled_dict_builder(menu_key))):
			title_item = compiled_dict_builder(menu_key)['select_' + str(items)]['title']
			menu_title_dict.update({'title_' + str(items): title_item})
		return menu_title_dict
	else:
		for items in range(len(compiled_dict_builder(menu_key))):
			print(compiled_dict_builder(menu_key)['select_' + str(items)]['title'])


def compiled_dict_builder(menu_key: str):
	"""
	If the passed in parameter from the local menu files passes a word that
	is not represented as the first word of a dict in class MenuDict.
	Ex: 'MAIN' calls the dict entry: 'MAIN_MENU_DICT'
	"""
	dict_name_builder = menu_key + '_MENU_DICT'  # Creates the dict name
	menu_dict_class = menu_dict.MenuDict()  # Instantiates class
	retrieved_dict = {}  # creates empty dict
	try:
		retrieved_dict.update({'select_0': initial_prompt_builder(menu_key)})  # adds header w/menu name to empty dict
		to_dict_name = getattr(menu_dict_class, dict_name_builder)# takes string dict name and makes it callable
		retrieved_dict.update(to_dict_name)  # combines dict w/select_0 w/rest of called dict
	except AttributeError:
		raise IndexError('The prefix: `{}` is not a menu dictionary'.format(dict_name_builder))
	return retrieved_dict # return whole dict

def initial_prompt_builder(menu_key: str):
	"""
	Inserts the 'menu_key' parameter into the menu header. Then the combined dict
	is added as 'select_0' to the dict class MenuDict
	"""
	title_text = '\n--~~$$ ' + menu_key + ' MENU $$~~--\nUse Numerical Keys To Select:\n'
	initial_prompt = {'title': 'initial_prompt_string', 'text': title_text, 'select_num': 0}
	return initial_prompt
