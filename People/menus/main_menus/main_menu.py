import sys

from People.menus.math_menus.math_menu import MathMenu
from People.menus.notes_menus.notes_menu import NotesMenu
from People.menus.menu_selection_dict import menu_builder


class MainMenu:
	def __init__(self):
		self.loop = True

	def menu_prompt(self):
		while self.loop:
			for items in range(len(menu_builder('MAIN'))):
				print(menu_builder('MAIN')['select_' + str(items)]['text'])
			select = int(input("Selection: "))
			if select == 1:
				MathMenu()
			elif select == 2:
				NotesMenu()
			elif select == 3:
				print('hi')
				# window()
			elif select == 4:
				print('END PROGRAM')
				soft_exit()
				self.loop = False
			else:
				print('Enter Proper Selection')
				self.loop = True


def soft_exit():
	select = input('--> ')
	if select == 'start':
		MainMenu()
	elif select == 'end':
		sys.exit('Later Mother Fucker')
	else:
		print('Incorrect :(')
		soft_exit()

__all__ = ['soft_exit','MainMenu']
