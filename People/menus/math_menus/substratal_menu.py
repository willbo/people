from People.menus.menu_selection_dict import menu_builder
from People.menus.main_menus.main_menu import soft_exit
from People.menus.math_menus.math_menu import MathMenu
from People.math.substratal import multiplication, subtraction, division, addition


class SubstratalMenu:
	def __init__(self):
		self.loop = True

	def menu_prompt(self):
		while self.loop:
			for items in range(len(menu_builder('SUBSTRATAL'))):
				print(menu_builder('SUBSTRATAL')['select_' + str(items)]['text'])
			select = int(input('Selection: '))
			if select == 1:
				addition.addition_header()
			elif select == 2:
				subtraction.subtraction_header()
			elif select == 3:
				multiplication.multiplication_header()
			elif select == 4:
				division.division_header()
			elif select == 5:
				self.loop = False
				print('Back')
				MathMenu()
			elif select == 6:
				self.loop = False
				print('Menu Exit')
				soft_exit()
			else:
				print('selection not avail')
				SubstratalMenu()
