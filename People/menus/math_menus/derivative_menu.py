from People.menus.menu_builder import menu_text_builder
from People.math.derivatives import derivative_prompt
from People.menus.main_menu import main_menu
from People.menus.math_menu import math_menu

MENU_KEY = 'DERIVATIVES'

class DerivativeMenu:
    def __init__(self):
        self.loop = True

    def menu_prompt(self):
        while self.loop:
            menu_text_builder(MENU_KEY)
            select = int(input('Selection: '))
            if select == 1:
                derivative_prompt()
            elif select == 2:
                self.loop = False
                print('Back')
                math_menu.MathMenu().menu_prompt()
            elif select == 3:
                self.loop = False
                print('Exit')
                main_menu.soft_exit()
            else:
                print('Select not avail')
                DerivativeMenu().menu_prompt()
