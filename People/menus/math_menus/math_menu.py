from People.menus.menu_selection_dict import menu_builder
from People.menus.main_menus.main_menu import soft_exit, MainMenu
from People.menus.math_menus.substratal_menu import SubstratalMenu
from People.menus.math_menus.derivative_menu import DerivativeMenu


class MathMenu:
	def __init__(self):
		self.loop = True

	def menu_prompt(self):
		while self.loop:
			for items in range(len(menu_builder('MATH'))):
				print(menu_builder('MATH')['select_' + str(items)]['text'])
			select = int(input('Selection: '))
			if select == 1:
				DerivativeMenu()
			elif select == 2:
				print('To be added: Integrals')
				soft_exit()
			elif select == 3:
				SubstratalMenu()
			elif select == 4:
				self.loop = False
				print('Back')
				MainMenu()
			elif select == 5:
				self.loop = False
				print('Menu Exit')
				soft_exit()
			else:
				print('selection not avail')
				MathMenu()
