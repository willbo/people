[![https://gitlab.com/willbo/people](
https://lh3.googleusercontent.com/qA_HGXeu8kKNDgXcfL1eh9jNGhkHY7Qc3512-jNwFC22ii9PzxM-XRkSPLtPIhJ9Ah83iOB4awao3cJ-sXO4A-M4yGSxrxnZ5Dvn9UV7Cw3K7SgBvd6VEUMX3c7lBLf6zmoThivyP_0W4rDGVLnUFNRVz8fEywaQiXul9o7cZxqAFYXeVIifBuIon9EEurWPBMqvZy7x-oK1jNYXwbcCQ6Tvg9g3ekA_26g4nCaqsNOkV9CRG6yFoGO_HnVqabF8bHr63Mb9UomBpLMV-CVEDFzDsysI0BS8hl0vIkKFbWbbbbzMbtPO8tb2bn7qXnLkytNwiEio9ewLjO9vRbMxup7GfiWTwpCD9FwC6H7OKDXsgKkY6SWRqTwso41xM1BICj6lwZQdPCA-vXUkHRYzMQEI45LPOplMsOaVtaotDPJr9N5CtqMbN4y9_DnXNZ0VaK0O3dWldHh0XWzfMElWI7BUkU3IGtrQJlZ4FFxxJGpI8shvSrjvQNQlMmdqyAq5E8hq6sFeEdP-vZgT3I-QAZeA1RzY3H0JuX06NlLor7QsMwuLiaTn6XNbQNpBMJSvou9v3M-sglwNTI89vdpTfJFrkspYFh4M75ozSItukDPLw-n2DzdVNYFr67aSivp_Wbl3B4AH_NWngZgoTSbEkylb0DK5Iw=w1280-h640-no 
"People By: Willbo")](https://gitlab.com/willbo/people)
#**People**
#####Description: 
- [People](https://gitlab.com/willbo/people) 
is meant to implement the ongoing education of the python3 language and object oriented programming
#####Contact:
- [will.gitlab@gmail.com](mailto:will.gitlab@gmail.com)
<!-- Comment
<br>PP PPPPPPP
<br>PPPP     PP
<br>PP        PP
<br>PP        PP  EEEEEEEEEE   OOOOOOOO    PP PPPPP    LL       EEEEEEEEEE
<br>PPP      PP   EE          OO      OO   PPP    PP   LL       EE
<br>PP PPPPPP     EE         OO        OO  PP      PP  LL       EE
<br>PP            EEEEEEE    OO        OO  PPP    PP   LL       EEEEEEEE
<br>PP            EE         OO        OO  PP PPPP     LL       EE
<br>PP            EE          OO      OO   PP          LL       EE
<br>PP            EEEEEEEEEE   OOOOOOOO    PP          LLLLLLLL EEEEEEEEEE
End Comment -->


##Install Instructions

####General Information
- Python Version: >= 3.7.0
- [GitLab Repository](https://gitlab.com/willbo/people): `https://gitlab.com/willbo/people`
- GitLab HTTPS Clone: `https://gitlab.com/willbo/people.git`
- Script Path: `~/People/People/run.py`
- Working Directory: `~/People/People`


####Terminal Instructions
- Install HomeBrew
  * `>>> /usr/bin/ruby -e "$(curl -fsSL https://tinyurl.com/oe9dlto)"`  
<br/>
- Install pip (probably not needed, read on)
  * `>>> sudo easy_install pip`
- Update pip
  * `>>> sudo pip install --upgrade pip`  
<br/>
- Or install python 3.7 and pip together(recommended) 
  * `>>> brew install python3`  
<br/>
- Move to directory where the file is to be cloned  
  * `>>> cd ~/example_dir`
    - Using: `~/` works from root directory.
- Clone git repo into the current directory
  * `>>> git clone https://gitlab.com/willbo/people.git`

####I.D.E. Settings
- Pycharm Virtual Environment
  * `Pycharm` >> `Preferences` >> `Project` >> `Project Interpreter`
  * Select the **options** (3 dots) button next to the `Project Interpreter` drop down
  * Select `Add`
  * Select the **folder icon** next to the `Location` drop down
  * Navigate to the **top level directory** the gitlab repo was cloned to (the first `People` directory) and select it
  * Under `Virtual Environment` >> `New Environment`, select the `Base Interpreter` drop down
  * Navigate to where the **python3.7** file was installed, which should be `usr/local/bin/python3.7`, and select it
  * Select `Apply` settings 
  * Select `OK`  
<br/>
- Pycharm Run Configuration
  * Select the `Add Configurations` drop down at the top of the window
  * Select the `+` button on the left hand side
  * Select `Python` in the resulting drop down
  * Select the **folder icon** to the right of the `Script Path` drop down
  * Navigate to **directory** where the **repo** was cloned
  * Select the **run.py** file under `People` >> `People` >> `run.py`
  * Select `Open`  
<br/>
  * Next, select the **folder icon** to the right of the `Working Directory` drop down
  * Navigate to **directory** where the **repo** was cloned
  * Select the **second** `Peoeple` directory
  * Select `Open`
  * Select `Apply` settings 
  * Select `OK`
  
####Run!!!
- Select the **green arrow** to the right of the `Run Configuration` drop down
- Type `start` to **begin** the program
- Follow the menu instructions
- Type `end` to completely **terminate** the program
- Enjoy dudezz
