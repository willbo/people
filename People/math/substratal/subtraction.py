def subtraction_prompt():
    loop = bool
    combined_number = []
    string_initial_number = input('Minuend number: ')
    int_initial_number = to_int(string_initial_number)
    print('Enter the subtrahend')
    next_string_number = input('Subtrahend number: ')
    next_int_number = to_int(next_string_number)
    combined_number.append(next_int_number)
    print(combined_number)
    while loop:
        next_string_number = input('Next subtrahend number: ')
        next_int_number = to_int(next_string_number)
        if next_int_number is not False:
            combined_number.append(next_int_number)
            print(combined_number)
        else:
            print('The Difference is: ' + str(subtraction(int_initial_number, *combined_number)))
            loop = False

def subtraction_header():
    print('\n--~~$$ SUBTRACTION MENU $$~~--'
		  '\nEnter any key other than a number to end'
		  '\nEnter the initial minuend number')
    subtraction_prompt()

def subtraction(initial_num, *args):
    for items in args:
        initial_num -= items
    return initial_num

def to_int(number):
    try:
        int(number)
        return int(number)
    except ValueError:
        return False
