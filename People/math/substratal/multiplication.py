def multiplication_prompt():
	loop = bool
	combined_number = []
	string_number = input('Multiplicand number:')
	int_number = to_int(string_number)
	combined_number.append(int_number)
	print('Enter the multiplier')
	next_string_number = input('Multiplier number:')
	next_int_number = to_int(next_string_number)
	combined_number.append(next_int_number)
	while loop:
		next_number = input('Next multiplier Number:')
		next_int_number = to_int(next_number)
		if next_int_number is not False:
			combined_number.append(next_int_number)
			print(combined_number)
		else:
			print('The Product is: ' + str(multiplication(*combined_number)))
			loop = False

def multiplication_header():
	print('\n--~~$$ MULTIPLICATION MENU $$~~--'
		  '\nEnter any key other than a number to end'
		  '\nEnter the initial multiplicand number')
	multiplication_prompt()

def multiplication(*args):
	multiplication_total = 1
	for items in args:
		multiplication_total *= items
	return multiplication_total

def to_int(number):
	try:
		int(number)
		return int(number)
	except ValueError:
		return False
