def addition_prompt():
    loop = bool
    combined_number = []
    string_number = input('Augend number: ')
    int_number = to_int(string_number)
    combined_number.append(int_number)
    print('Enter the addend')
    next_string_number = input('Addend number: ')
    next_int_number = to_int(next_string_number)
    combined_number.append(next_int_number)
    print(combined_number)
    while loop:
        next_string_number = input('Next addend number: ')
        next_int_number = to_int(next_string_number)
        if next_int_number is not False:
            combined_number.append(next_int_number)
            print(combined_number)
        else:
            print('The Sum is: ' + str(addition(*combined_number)))
            loop = False

def addition_header():
    print('\n--~~$$ ADDITION MENU $$~~--'
		  '\nEnter any key other than a number to end'
		  '\nEnter the initial augend number')
    addition_prompt()

def addition(*args):
    addition_total = 0
    for items in args:
        addition_total += items
    return addition_total

def to_int(number):
    try:
        int(number)
        return int(number)
    except ValueError:
        return False
