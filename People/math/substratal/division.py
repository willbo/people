def division_prompt():
	loop = bool
	combined_number = []
	string_initial_number = input('Dividend number: ')
	int_initial_number = to_int(string_initial_number)
	print('Enter the divisor')
	next_string_number = input('Divisor number: ')
	next_int_number = to_int(next_string_number)
	combined_number.append(next_int_number)
	print(combined_number)
	while loop:
		next_string_number = input('Next divisor number: ')
		next_int_number = to_int(next_string_number)
		if next_int_number is not False:
			combined_number.append(next_int_number)
			print(combined_number)
		else:
			print('The Quotient is: ' + str(division(int_initial_number, *combined_number)))
			loop = False

def division_header():
	print('\n--~~$$ DIVISION MENU $$~~--'
		  '\nEnter any key other than a number to end'
		  '\nEnter the initial dividend number')
	division_prompt()

def division(initial_num, *args):
	for items in args:
		initial_num = initial_num/items
	return initial_num

def to_int(number):
	try:
		int(number)
		return int(number)
	except ValueError:
		return False
