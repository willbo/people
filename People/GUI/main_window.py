import tkinter as tk
from tkinter import font  as tk_font
# from People.menus.menu_builder import compiled_dict_builder

class GlobalExit:
    def __init__(self,  controller):
        self.controller = controller
        exit_button = tk.Button(text='Exit People',
                               command=lambda: self.controller.destroy())
        exit_button.pack(side='bottom')

class PeopleGUI(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        self.title_font = tk_font.Font(family='Helvetica',size=14,weight="bold",slant="italic")

        # the container is where we'll stack a bunch of frames
        # on top of each other, then the one we want visible
        # will be raised above the others
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for items in (MainMenuWindow, MathMenuWindow, NotesMenuWindow, MultiplicationWindow):
            page_name = items.__name__
            frame = items(parent=container, controller=self)
            self.frames[page_name] = frame

            # put all of the pages in the same location;
            # the one on the top of the stacking order
            # will be the one that is visible.
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame("MainMenuWindow")

    def show_frame(self, page_name):
        """
        Show a frame for the given page name
        """
        frame = self.frames[page_name]
        frame.tkraise()


class MainMenuWindow(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text='Main Menu', font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)

        math_menu_button = tk.Button(self, text="Math Menu",
                                     command=lambda: controller.show_frame("MathMenuWindow"))
        notes_menu_button = tk.Button(self, text="Notes Menu",
                                      command=lambda: controller.show_frame("NotesMenuWindow"))
        math_menu_button.pack(side='top', anchor='w', fill='y', expand='no')
        notes_menu_button.pack(side='top', anchor='w', fill='y', expand='no')
        GlobalExit(self.controller)


class MathMenuWindow(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Math Menu", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        multiplication_button = tk.Button(self, text='Multiplication',
                                          command=lambda: controller.show_frame('MultiplicationWindow'))
        multiplication_button.pack(side='top', anchor='w', fill='y', expand='no')
        division_button = tk.Button(self, text='Division',
                                    command=lambda: controller.show_frame('DivisionWindow'))
        division_button.pack(side='top', anchor='w', fill='y', expand='no')
        back_button = tk.Button(self, text="Back",
                           command=lambda: controller.show_frame("MainMenuWindow"))
        back_button.pack(side='top', anchor='w', fill='y', expand='no')
        # for items in range(len(compiled_dict_builder('MATH'))):
        #     maths_buttons = tk.Button(self, text=compiled_dict_builder('MATH')['select_' + str(items)]['text'],
        #                               command=lambda: controller.show_frame("MultiplicationMenuWindow"))
        #     maths_buttons.pack(side='top', anchor='w', fill='y', expand='no')

class MultiplicationWindow(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text='Multiplication Menu', font=controller.title_font)
        label.pack(side='top', fill='x', pady=10)
        entry = tk.Entry(self)
        self.entry = entry
        entry.insert(0, 'Multiplication')
        entry.pack(padx=10)
        # entry.bind("<ButtonPress>",MultiplicationWindow.delete_entry_text(event="<Button-1>"))
        # window_init = tk.Frame(self, bd=2, relief='ridge')
        # entry_window = tk.Entry(self, window_init, text_variable=tk.StringVar(), bg='white')
        # entry_window.pack()
        # tk.StringVar().set('First Number')
        # window_init.pack(expand=.5, fill='x', pady=10, padx=10)

        back_button = tk.Button(self,text="Back",
                                command=lambda: controller.show_frame('MathMenuWindow'))
        back_button.pack(side='top',anchor='w',fill='y',expand='no')

    # def delete_entry_text(self, event):
    #     self.entry.delete(0, 'end')

class NotesMenuWindow(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Notes Menu", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        back_button = tk.Button(self, text="Back",
                                command=lambda: controller.show_frame("MainMenuWindow"))
        back_button.pack(side='left')


if __name__ == "__main__":
    root = PeopleGUI()
    root.geometry('600x600')
    root.mainloop()
