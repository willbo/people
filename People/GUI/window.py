from tkinter import *
from People.menus.math_menu.math_menu import MathMenu


class WindowStart:
	def __init__(self, window_attribute):
		self.window_attribute = window_attribute
		window_attribute.title = 'KNOWLEDGE'

		self.label = Label(window_attribute, text='Label')
		self.label.pack()

		self.menu_button = Button(window_attribute, text='Math', command=self.math_button)
		self.menu_button.pack()

		self.terminate_button = Button(window_attribute, text='End', command=window_attribute.quit)
		self.terminate_button.pack()

		self.entry_prompt = Entry(root, text='Enter Name')

		self.name(side=LEFT)

	def math_button(self):
		print('opens math')
		MathMenu.menu_prompt()

	def name(self, name):
		print(name)

root = Tk()
gui_start = WindowStart(root)
root.mainloop()
